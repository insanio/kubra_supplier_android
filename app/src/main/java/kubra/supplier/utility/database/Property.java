package kubra.supplier.utility.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.sql.SQLException;

import kubra.supplier.service.LocalDatabase;

public class Property {

    public static final int createDDLType = 0, dropDDLType = 1;
    private User user;
    private Asset asset;

    public Property() {
        this.user = new User();
        this.asset = new Asset();
    }

    public void createAllTable(SQLiteDatabase db) {
        db.execSQL(this.user.getDDL(createDDLType));
        db.execSQL(this.asset.getDDL(createDDLType));
    }

    public void dropAllTable(SQLiteDatabase db) {
        db.execSQL(this.user.getDDL(dropDDLType));
        db.execSQL(this.asset.getDDL(dropDDLType));
    }

    public void deleteAllTableData(LocalDatabase localDatabase) {
        SQLiteDatabase db = localDatabase.getWritableDatabase();
        try {
            db.beginTransaction();

            localDatabase.deleteData(this.user.getName(), "", new String[]{}, true);
            localDatabase.deleteData(this.asset.getName(), "", new String[]{}, true);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void checkSQLTable(SQLiteDatabase db, String name) {
        if (name.equals(this.user.getName()))
            db.execSQL(this.user.getDDL(createDDLType));
        else if (name.equals(this.asset.getName()))
            db.execSQL(this.asset.getDDL(createDDLType));
    }
}

class AppBaseColumns implements BaseColumns {
    String columnUpdated = "last_updated";

    protected String[] setDdl(String name, String[] column) {
        StringBuilder create = new StringBuilder();
        create.append(" " + _ID + " integer primary key autoincrement ");
        for (String col:column) {
            create.append(" , " + col + " text ");
        }
        create.insert(0, " ( ").insert(0, name).insert(0, " create table if not exists ").append(" ) ");

        StringBuilder drop = new StringBuilder();
        drop.append(" drop table ").append(name);

        return new String[]{create.toString(), drop.toString()};
    }
}

class User extends AppBaseColumns {

    private String[] column, ddl;
    private String name;

    User() {
        this.name = "user";
        this.column = new String[]{"server_id", "username", "name", "phone", "mobile", "email", "location", "address", "description", super.columnUpdated};

        this.ddl = super.setDdl(this.name, this.column);
    }

    protected String getName() {
        return this.name;
    }

    protected String[] getColumns() {
        return this.column;
    }

    protected String getDDL(int ddlType) {
        if (ddlType < this.ddl.length)
            return this.ddl[ddlType];
        else
            return null;
    }
}

class Asset extends AppBaseColumns {

    private String[] column, ddl;
    private String name;

    Asset() {
        this.name = "asset";
        this.column = new String[]{"server_id", "server_user_id", "server_user_username", "asset_type", "asset_type_name", "asset_type_code", "name", "phone", "mobile", "email", "address", "latitude", "longitude", "description", super.columnUpdated};

        this.ddl = super.setDdl(this.name, this.column);
    }

    protected String getName() {
        return this.name;
    }

    protected String[] getColumns() {
        return this.column;
    }

    protected String getDDL(int ddlType) {
        if (ddlType < this.ddl.length)
            return this.ddl[ddlType];
        else
            return null;
    }
}