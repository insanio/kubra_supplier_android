package kubra.supplier.utility.slider.image;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.List;

public class Adapter extends FragmentStatePagerAdapter {

    private static final String TAG = "SliderPagerAdapter";

    List<String> images;

    public Adapter(FragmentManager fm, List<String> images) {
        super(fm);
        this.images = images;
    }

    @Override
    public Fragment getItem(int position) {
        int index = position % this.images.size();
//        Log.wtf("Slider Adapter", "Indexes: " + position + ", " + index);
        return Fragment.newInstance(this.images.get(index));
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}