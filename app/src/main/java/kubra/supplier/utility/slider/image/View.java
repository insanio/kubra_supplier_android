package kubra.supplier.utility.slider.image;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

public class View extends ViewPager {

    private static int defaultScrollDuration = 1000;

    public View(@NonNull Context context, int scrollDuration) {
        super(context);

        this.init(scrollDuration);
    }

    public View(@NonNull Context context, @Nullable AttributeSet attrs, int scrollDuration) {
        super(context, attrs);

        this.init(scrollDuration);
    }

    public View(@NonNull Context context) {
        super(context);

        this.init(defaultScrollDuration);
    }

    public View(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.init(defaultScrollDuration);
    }

    private void init(int scrollDuration) {
        setDurationScroll(scrollDuration);
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(android.view.View v, MotionEvent event) {
                return true;
            }
        });
    }
    public void setDurationScroll(int millis) {
        try {
            Class<?> viewpager = ViewPager.class;
            Field scroller = viewpager.getDeclaredField("mScroller");
            scroller.setAccessible(true);
            scroller.set(this, new OwnScroller(getContext(), millis));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class OwnScroller extends Scroller {

        private int durationScrollMillis = 1;

        public OwnScroller(Context context, int durationScroll) {
            super(context, new DecelerateInterpolator());
            this.durationScrollMillis = durationScroll;
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, durationScrollMillis);
        }
    }
}
