package kubra.supplier.utility.slider.image;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import kubra.supplier.R;

public class Fragment extends android.support.v4.app.Fragment {

    private static final String urlParam = "image_url";

    private String imageUrls;

    public Fragment() {
    }

    public static Fragment newInstance(String url) {
        Fragment fragment = new Fragment();
        Bundle args = new Bundle();
        args.putString(urlParam, url);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.imageUrls = this.getArguments().getString(urlParam);

        android.view.View view = inflater.inflate(R.layout.utility_fragment_sliderpager_item, container, false);
        ImageView img = view.findViewById(R.id.image);
        Glide.with(this.getActivity()).load(this.imageUrls).into(img);
        return view;
    }
}
