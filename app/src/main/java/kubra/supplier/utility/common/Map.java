package kubra.supplier.utility.common;

import org.json.JSONObject;

public class Map {

    public static Object validateMap(java.util.Map<String, Object> map, String key) {
        Object ret;

        ret = map.containsKey(key) && map.get(key) != null ? map.get(key) : "";

        return ret;
    }
}
