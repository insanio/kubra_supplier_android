package kubra.supplier.utility.common;

import android.content.Context;
import android.util.DisplayMetrics;

public class Screen {

    public static int getScreenWidthDp(Context context, DisplayMetrics displayMetrics) {
        return (int) (getScreenWidthPx(context) / displayMetrics.density);
    }

    public static int getScreenHeightDp(Context context, DisplayMetrics displayMetrics) {
        return (int) (getScreenHeightPx(context) / displayMetrics.density);
    }

    public static int getScreenWidthPx(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeightPx(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }
}
