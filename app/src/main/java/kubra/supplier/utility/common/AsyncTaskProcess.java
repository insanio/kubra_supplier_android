package kubra.supplier.utility.common;


import android.os.AsyncTask;

import java.util.List;

public class AsyncTaskProcess extends AsyncTask<Void, Void, Void> {
    private TaskProcess process;
    private List<AsyncTaskProcess> threads;

    public AsyncTaskProcess(TaskProcess process, List<AsyncTaskProcess> threadPool) {
        this.process = process;
        this.threads = threadPool;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        this.process.process();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (this.threads != null) {
            this.threads.remove(this);
        }
    }

    public interface TaskProcess{
        void process();
    }
}