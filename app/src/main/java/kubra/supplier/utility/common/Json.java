package kubra.supplier.utility.common;

import org.json.JSONException;
import org.json.JSONObject;

public class Json {

    public static Object validateJsonObject(JSONObject object, String key) throws JSONException {
        Object ret;

        ret = object.has(key) && object.get(key) != JSONObject.NULL ? object.get(key) : "";

        return ret;
    }
}
