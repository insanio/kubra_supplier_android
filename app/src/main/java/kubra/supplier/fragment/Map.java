package kubra.supplier.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.clustering.ClusterManager;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import kubra.supplier.R;
import kubra.supplier.activity.Asset;
import kubra.supplier.activity.Auth;
import kubra.supplier.service.Http;
import kubra.supplier.utility.common.AsyncTaskProcess;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnMapFragmentAction} interface
 * to handle interaction events.
 * Use the {@link Map#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Map extends Fragment implements OnMapReadyCallback {

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        this.updateMap();

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        this.map.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        this.map.moveCamera(CameraUpdateFactory.newLatLng(sydney));

//        this.map.setMyLocationEnabled(true);
//        this.map.getUiSettings().setMyLocationButtonEnabled(true);

//        this.map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
//            @Override
//            public void onCameraIdle() {
//                LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
//
//                Log.wtf("Map fragment", "Bounds: " + bounds);
//            }
//        });

        this.clusterManager = new ClusterManager<>(this.getContext(), this.map);

        this.clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterItem>() {
            @Override
            public boolean onClusterItemClick(Map.ClusterItem clusterItem) {
                reCluster = false;

                return false;
            }
        });

        this.map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
                    @Override
                    public void process() {
                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getItem(reCluster);
                                clusterManager.onCameraIdle();

                                if (!reCluster)
                                    reCluster = true;
                            }
                        });
                    }
                }, threads);

                threads.add(thread);
                thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
//        this.map.setOnCameraIdleListener(this.clusterManager);
        this.map.setOnMarkerClickListener(this.clusterManager);
    }

    private boolean reCluster = true;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";

    private GoogleMap map;
    private ClusterManager<ClusterItem> clusterManager;
//    private FusedLocationProviderClient mFusedLocationProviderClient;
    private int currentMapZoomLevel;

    private Asset mainActivity;
    private List<AsyncTaskProcess> threads;

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    private OnMapFragmentAction mListener;

    public Map() {
        // Required empty public constructor
        this.threads = new ArrayList<>();
        this.currentMapZoomLevel = 15;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     //     * @param param1 Parameter 1.
     //     * @param param2 Parameter 2.
     * @return A new instance of fragment Map.
     */
    // TODO: Rename and change types and number of parameters
    public static Map newInstance() {
        Map fragment = new Map();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        this.initView();

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onMapAction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMapFragmentAction) {
            mListener = (OnMapFragmentAction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMapFragmentAction");
        }
    }

    @Override
    public void onDetach() {
        this.stopAllThreads();

        super.onDetach();
        this.mListener = null;
    }

    @Override
    public void onDestroy() {
        this.stopAllThreads();

        super.onDestroy();
    }

    private void stopAllThreads() {
        for (AsyncTaskProcess thread : this.threads) {
            if (!thread.isCancelled())
                thread.cancel(true);
        }

        this.threads.clear();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMapFragmentAction {
        // TODO: Update argument type and name
        void onMapAction(String uri);
    }

    private void initView() {
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        this.mainActivity = (Asset) this.getActivity();
    }

    public void updateMap() {
        if (this.map != null) {
            try {
                Log.wtf("UpdateMap", "Access: " + this.mainActivity.mapAccessFineLocation);

                if (this.mainActivity.mapAccessFineLocation) {
                    this.map.setMyLocationEnabled(true);
                    this.map.getUiSettings().setMyLocationButtonEnabled(true);

                    this.getDeviceLocation();
                } else {
                    this.map.setMyLocationEnabled(false);
                    this.map.getUiSettings().setMyLocationButtonEnabled(false);
//                mLastKnownLocation = null;
                    this.mainActivity.getLocationPermission();
                }
            } catch (SecurityException e) {
                Log.e("Exception: %s", e.getMessage());
            }
        }
    }

    private void getDeviceLocation() {
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LocationManager locationManager = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
                        Location location = getLastKnownLocation(locationManager);

                        if (location != null)
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), currentMapZoomLevel));
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private Location getLastKnownLocation(LocationManager locationManager) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        List<String> providers = locationManager.getProviders(criteria, false);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }

//            Log.wtf("Map Fragment", "Best location: " + l);

            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }

        return bestLocation;
    }

//    private void getDeviceLocation() {
//        try {
//            if (this.mainActivity.mapAccessFineLocation) {
//                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
//                locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Location> task) {
//                        if (task.isSuccessful()) {
//                            // Set the map's camera position to the current location of the device.
//                            Location location = task.getResult();
//                            LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
//                            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng, currentMapZoomLevel);
//                            map.moveCamera(update);
//                        }
//                    }
//                });
//            }
//        } catch (SecurityException e) {
//            Log.e("Exception: %s", e.getMessage());
//        }
//    }

    private List<ClusterItem> dummyItem = new ArrayList<>();

    private void getItem(boolean reCluster) {
//        Log.wtf("Map Fragment", "Get item: " + reCluster);
        if (reCluster) {
            LatLngBounds latLngBounds = this.map.getProjection().getVisibleRegion().latLngBounds;

            JSONObject request = new JSONObject();
            try {
                JSONObject northeast = new JSONObject();
                northeast.put("latitude", Double.toString(latLngBounds.northeast.latitude));
                northeast.put("longitude", Double.toString(latLngBounds.northeast.longitude));

                JSONObject southwest = new JSONObject();
                southwest.put("latitude", Double.toString(latLngBounds.southwest.latitude));
                southwest.put("longitude", Double.toString(latLngBounds.southwest.longitude));

                request.put("northeast", northeast);
                request.put("southwest", southwest);

                StringEntity entity = new StringEntity(request.toString());

//                Log.wtf("Map Fragment", "Request Entity: " + entity);
//                Log.wtf("Map Fragment", "Request JSON: " + request);

//                Http.post(this.getContext(), "android/getAssetLocation", entity, "application/json", new JsonHttpResponseHandler() {
                Http.appPostJsonResponseJson(this.getContext(), "android/getAssetLocation", entity, "application/json", new Http.AppHttpResponse() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, Object success) {
                        setCluster((JSONObject) success);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                        Http.failedJsonResponseAction(getContext(), throwable, "Ooops... Server seems to be having a problem processing this request...", failure);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void setCluster(JSONObject item) {
        List<ClusterItem> items = new ArrayList<>();
        try {
            JSONArray data = item.getJSONObject("data").getJSONArray("data");
            for (int i=0;i<data.length();i++) {
                JSONObject asset = data.getJSONObject(i);
                ClusterItem clusterItem = new ClusterItem(asset.getDouble("lat_value"), asset.getDouble("lng_value"), asset.getString("asset_name"), asset.getString("note"), asset.getString("icon_value"));
                items.add(clusterItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (items.size() > 0) {
            this.clusterManager.clearItems();
            this.clusterManager.addItems(items);
//            this.clusterManager.setRenderer(new ClusterRenderer(this.getContext(), this.map, this.clusterManager));
            this.clusterManager.cluster();

//            Log.wtf("Map Fragment", "Total items: " + items.size() + ", " + items);
        }
    }

    private class ClusterItem implements com.google.maps.android.clustering.ClusterItem {
        private final LatLng mPosition;
        private final String mTitle;
        private final String mSnippet;
        private final String iconUrl;

        public ClusterItem(double lat, double lng) {
            this.mPosition = new LatLng(lat, lng);
            this.mTitle = "";
            this.mSnippet = "";
            this.iconUrl = "";
        }

        public ClusterItem(double lat, double lng, String title, String snippet, String iconUrl) {
            this.mPosition = new LatLng(lat, lng);
            this.mTitle = title;
            this.mSnippet = snippet;
            this.iconUrl = iconUrl;
        }

        @Override
        public LatLng getPosition() {
            return this.mPosition;
        }

        @Override
        public String getTitle() {
            return this.mTitle;
        }

        @Override
        public String getSnippet() {
            return this.mSnippet;
        }

        public String getIconUrl() {
            return this.iconUrl;
        }
    }

//    private class ClusterRenderer extends DefaultClusterRenderer<ClusterItem> {
//
//        public ClusterRenderer(Context context, GoogleMap map, ClusterManager<ClusterItem> clusterManager) {
//            super(context, map, clusterManager);
//        }
//
//        @Override
//        protected void onBeforeClusterItemRendered(final ClusterItem item, final MarkerOptions markerOptions) {
//            if (item.getIconUrl() != null && !item.getIconUrl().equals("")) {
//                try {
//                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Http.streamBitmapSync(item.getIconUrl())));
//                } catch (ExecutionException | InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            new Http.StreamBitmapAsync(new Http.StreamBitmapAsyncResponse() {
//                @Override
//                public void setBitmap(Bitmap bitmap) {
//                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
//
//                    Log.wtf("Map Fragment", "Set bitmap: " + item.getIconUrl());
//                }
//            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, item.getIconUrl());
//
//            markerOptions.title(item.getTitle());
//            markerOptions.snippet(item.getSnippet());
//            markerOptions.position(item.getPosition());
//
//            super.onBeforeClusterItemRendered(item, markerOptions);
//        }
//    }
}

// Set some lat/lng coordinates to start with.
//        LatLng position = this.map.getCameraPosition().target;
//        double lat = position.latitude;
//        double lng = position.longitude;

// Add ten cluster items in close proximity, for purposes of this example.
//        if (this.dummyItem.size() > 0) {
////            this.clusterManager.clearItems();
//            for (int i=0;i<this.dummyItem.size();i++) {
//                this.clusterManager.removeItem(this.dummyItem.get(i));
//            }
//
//            Log.wtf("Map Fragment", "Clear item: " + this.dummyItem);
//
//            this.clusterManager.cluster();
//        } else

//    LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
//    Random r = new Random();
//
//        this.clusterManager.clearItems();
//                for (int i = 0; i < 10; i++) {
//        double lat = bounds.southwest.latitude + (bounds.northeast.latitude - bounds.southwest.latitude) * r.nextDouble();
//        double lng = bounds.southwest.longitude + (bounds.northeast.longitude - bounds.southwest.longitude) * r.nextDouble();
//        this.dummyItem.add(new ClusterItem(lat, lng, "Title " + (i + 1), "Snippet " + (i + 1)));
//        this.clusterManager.addItem(this.dummyItem.get(this.dummyItem.size() - 1));
//        }
//
//        if (reCluster)
//        this.clusterManager.cluster();
//
//        Log.wtf("Map Fragment", "Get item: " + bounds);