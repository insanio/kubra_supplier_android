package kubra.supplier.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import kubra.supplier.R;
import kubra.supplier.utility.common.Screen;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnProductFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Product#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Product extends Fragment {

    private OnProductFragmentInteractionListener mListener;
    private View v;
    private LinearLayout containerAsset;
    private TextView textUsername, textName, textPhone, textMobile, textEmail, textAddress;
    private CircleImageView imageUser;

    private List<ProductAssetViewHolder> holders;

    public Product() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Product.
     */
    // TODO: Rename and change types and number of parameters
    public static Product newInstance() {
        Product fragment = new Product();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_product, container, false);

        this.containerAsset = this.v.findViewById(R.id.asset_container);

        this.imageUser = this.v.findViewById(R.id.image_user);
        this.imageUser.getLayoutParams().height = Screen.getScreenWidthPx(this.getContext());

        this.textUsername = this.v.findViewById(R.id.textview_username);
        this.textName = this.v.findViewById(R.id.textview_name);
        this.textPhone = this.v.findViewById(R.id.textview_phone);
        this.textMobile = this.v.findViewById(R.id.textview_mobile);
        this.textEmail = this.v.findViewById(R.id.textview_email);
        this.textAddress = this.v.findViewById(R.id.textview_address);

        this.holders = new ArrayList<>();

        this.mListener.afterProductViewInit();

        return this.v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProductFragmentInteractionListener) {
            mListener = (OnProductFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setProduct(Map<String, Object> product) {
        this.textUsername.setText(product.containsKey("username") ? product.get("username").toString() : "");
        this.textName.setText(product.containsKey("name") ? product.get("name").toString() : "");
        this.textPhone.setText(product.containsKey("phone") ? product.get("phone").toString() : "");
        this.textMobile.setText(product.containsKey("mobile") ? product.get("mobile").toString() : "");
        this.textEmail.setText(product.containsKey("email") ? product.get("email").toString() : "");
        this.textAddress.setText(product.containsKey("address") ? product.get("address").toString() : "");
    }

//    public void setAsset(List<Object> assets) {
//        this.containerAsset.removeAllViews();
//
//        for (Object asset:assets) {
//            if (asset instanceof Map) {
////                Log.wtf("Product Fragment", "Asset: " + asset);
//                Map<String, Object> userAsset = (Map<String, Object>) asset;
//
//                ProductAssetViewHolder view = new ProductAssetViewHolder(LayoutInflater.from(this.getContext()).inflate(R.layout.fragment_product_asset_list, null, false));
//                view.setName(userAsset.get("name") == null ? "" : userAsset.get("name").toString());
//                view.setDescription(userAsset.get("description") == null ? "" : userAsset.get("description").toString());
//
//                this.holders.add(view);
//            }
//        }
//
//        for (ProductAssetViewHolder holder:this.holders) {
////            Log.wtf("Product Fragment", "Holder: " + holder.getName());
//            this.containerAsset.addView(holder.getView());
//        }
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProductFragmentInteractionListener {
        // TODO: Update argument type and name
        void afterProductViewInit();
    }
}

class ProductAssetViewHolder {

    private View v;
    private TextView textName, textDescription;

    public ProductAssetViewHolder(View v) {
        this.v = v;

        this.initView();
    }

    private void initView() {
        this.textName = this.v.findViewById(R.id.name);
        this.textDescription = this.v.findViewById(R.id.description);
    }

    protected void setName(String name) {
        this.textName.setText(name);
    }

    protected String getName() {
        return this.textName.getText().toString();
    }

    protected void setDescription(String description) {
        this.textDescription.setText(description);
    }

    protected String getDescription() {
        return this.textDescription.getText().toString();
    }

    protected View getView() {
        return this.v;
    }
}