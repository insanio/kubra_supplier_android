package kubra.supplier.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import kubra.supplier.R;
import kubra.supplier.utility.common.Screen;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnStoreFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Store#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Store extends Fragment {

    private OnStoreFragmentInteractionListener mListener;
    private View v;
    private LinearLayout containerAsset;
    private TextView textUsername, textName, textPhone, textMobile, textEmail, textAddress;
    private CircleImageView imageUser;

    private List<StoreAssetViewHolder> holders;

    public Store() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Store.
     */
    // TODO: Rename and change types and number of parameters
    public static Store newInstance() {
        Store fragment = new Store();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_store, container, false);

        this.containerAsset = this.v.findViewById(R.id.asset_container);

        this.imageUser = this.v.findViewById(R.id.image_user);
        this.imageUser.getLayoutParams().height = Screen.getScreenWidthPx(this.getContext());

        this.textUsername = this.v.findViewById(R.id.textview_username);
        this.textName = this.v.findViewById(R.id.textview_name);
        this.textPhone = this.v.findViewById(R.id.textview_phone);
        this.textMobile = this.v.findViewById(R.id.textview_mobile);
        this.textEmail = this.v.findViewById(R.id.textview_email);
        this.textAddress = this.v.findViewById(R.id.textview_address);

        this.holders = new ArrayList<>();

        this.mListener.afterStoreViewInit();

        return this.v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStoreFragmentInteractionListener) {
            mListener = (OnStoreFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setStore(Map<String, Object> store) {
        this.textUsername.setText(store.containsKey("username") ? store.get("username").toString() : "");
        this.textName.setText(store.containsKey("name") ? store.get("name").toString() : "");
        this.textPhone.setText(store.containsKey("phone") ? store.get("phone").toString() : "");
        this.textMobile.setText(store.containsKey("mobile") ? store.get("mobile").toString() : "");
        this.textEmail.setText(store.containsKey("email") ? store.get("email").toString() : "");
        this.textAddress.setText(store.containsKey("address") ? store.get("address").toString() : "");
    }

    public void setAsset(List<Object> assets) {
        this.containerAsset.removeAllViews();

        for (Object asset:assets) {
            if (asset instanceof Map) {
//                Log.wtf("Store Fragment", "Asset: " + asset);
                Map<String, Object> userAsset = (Map<String, Object>) asset;

                final StoreAssetViewHolder view = new StoreAssetViewHolder(LayoutInflater.from(this.getContext()).inflate(R.layout.fragment_profile_asset_list, null, false));
                view.setName(userAsset.get("name") == null ? "" : userAsset.get("name").toString());
                view.setDescription(userAsset.get("description") == null ? "" : userAsset.get("description").toString());
                view.setId(userAsset.get("server_id") == null ? "" : userAsset.get("server_id").toString());
                view.getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onStoreAssetClicked(view.getId());
                    }
                });

                this.holders.add(view);
            }
        }

        for (StoreAssetViewHolder holder:this.holders) {
//            Log.wtf("Store Fragment", "Holder: " + holder.getName());
            this.containerAsset.addView(holder.getView());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStoreFragmentInteractionListener {
        // TODO: Update argument type and name
        void afterStoreViewInit();
        void onStoreAssetClicked(String id);
    }
}

class StoreAssetViewHolder {

    private View v;
    private TextView textName, textDescription;
    private String id;

    public StoreAssetViewHolder(View v) {
        this.v = v;

        this.initView();
    }

    private void initView() {
        this.textName = this.v.findViewById(R.id.name);
        this.textDescription = this.v.findViewById(R.id.description);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    protected void setName(String name) {
        this.textName.setText(name);
    }

    protected String getName() {
        return this.textName.getText().toString();
    }

    protected void setDescription(String description) {
        this.textDescription.setText(description);
    }

    protected String getDescription() {
        return this.textDescription.getText().toString();
    }

    protected View getView() {
        return this.v;
    }
}