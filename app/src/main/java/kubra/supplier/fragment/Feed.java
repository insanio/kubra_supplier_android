package kubra.supplier.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kubra.supplier.R;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFeedFragmentAction}
 * interface.
 */
public class Feed extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnFeedFragmentAction mListener;

    private List<kubra.supplier.item.Feed> item;
    private FeedAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Feed() {
        this.item = new ArrayList<>();
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static Feed newInstance() {
        Feed fragment = new Feed();
        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
//            this.mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (this.mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            this.adapter = new FeedAdapter(this.item, this.mListener);
            recyclerView.setAdapter(this.adapter);
        }

        this.mListener.afterFeedViewInit();

        return view;
    }

    public void setItem(List<Object> items) {
        this.item.clear();

        for (Object item:items) {
            if (item instanceof Map) {
                Map<String, Object> itemMap = (Map<String, Object>) item;

                kubra.supplier.item.Feed feed = new kubra.supplier.item.Feed();
                feed.setTitle(itemMap.get("asset_type_name").toString() + " " + itemMap.get("name").toString());
                feed.setId(kubra.supplier.utility.common.Map.validateMap(itemMap, "server_id").toString());
                feed.setType_code(kubra.supplier.utility.common.Map.validateMap(itemMap, "asset_type_code").toString());
                feed.setType_id(kubra.supplier.utility.common.Map.validateMap(itemMap, "asset_type_id").toString());
                feed.setDescription("Deskripsi " + itemMap.get("name").toString());

                this.item.add(feed);
            }
        }

        Log.wtf("Feed Fragment", "Set item: " + this.item);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFeedFragmentAction) {
            this.mListener = (OnFeedFragmentAction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFeedFragmentAction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFeedFragmentAction {
        // TODO: Update argument type and name
        void onFeedListSelected(kubra.supplier.item.Feed item);
        void afterFeedViewInit();
    }
}

/**
 * {@link RecyclerView.Adapter} that can display a {@link kubra.supplier.item.Feed} and makes a call to the
 * specified {@link Feed.OnFeedFragmentAction}.
 * TODO: Replace the implementation with code for your data type.
 */
class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {

    private final List<kubra.supplier.item.Feed> mValues;
    private final Feed.OnFeedFragmentAction mListener;

    public FeedAdapter(List<kubra.supplier.item.Feed> items, Feed.OnFeedFragmentAction listener) {
        this.mValues = items;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.nameView.setText(mValues.get(position).getTitle().trim());
        holder.mContentView.setText(mValues.get(position).getDescription().trim());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onFeedListSelected(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final TextView mContentView;
        public kubra.supplier.item.Feed mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameView = view.findViewById(R.id.title);
            mContentView = view.findViewById(R.id.description);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

