package kubra.supplier.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import kubra.supplier.R;
import kubra.supplier.activity.Asset;
import kubra.supplier.utility.common.Screen;
import kubra.supplier.utility.slider.image.Adapter;
import kubra.supplier.utility.slider.image.Indicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Main.OnMainFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Main#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Main extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters

    private OnMainFragmentInteractionListener mListener;
    private ConstraintLayout sliderContainer;
    private kubra.supplier.utility.slider.image.View slider;
    private LinearLayout indicatorContainer;
    private Indicator indicator;
    private Adapter adapter;
    private Button buttonSearchAsset;
    private View v;

    public Main() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Main.
     */
    // TODO: Rename and change types and number of parameters
    public static Main newInstance() {
        Main fragment = new Main();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_main, container, false);

        this.sliderContainer = v.findViewById(R.id.slider_container);
        this.slider = v.findViewById(R.id.slider);
        this.indicatorContainer = v.findViewById(R.id.indicator_container);

        this.sliderContainer.getLayoutParams().height = Screen.getScreenHeightPx(this.getContext()) / 3;
//        Log.wtf("Main Fragment", "Height: " + this.sliderContainer.getLayoutParams().height);

        this.buttonSearchAsset = v.findViewById(R.id.button_search_asset);
        this.buttonSearchAsset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAssetActivity();
            }
        });

        this.mListener.afterMainViewInit();

        return this.v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Object object) {
        if (mListener != null) {
            mListener.onMainFragmentInteraction(object);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMainFragmentInteractionListener) {
            mListener = (OnMainFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setSliderImage(List<String> images) {
//        List<kubra.supplier.utility.slider.image.Fragment> fragments = new ArrayList<>();
//        for (String image:images) {
//            fragments.add(kubra.supplier.utility.slider.image.Fragment.newInstance(image));
//        }

//        this.clearFragments(fragments);
        if (this.adapter == null) {
            this.adapter = new Adapter(this.getChildFragmentManager(), images);
            this.slider.setAdapter(this.adapter);
        }
        this.adapter.setImages(images);

        if (this.indicator == null)
            this.indicator = new Indicator(this.getContext(), this.indicatorContainer, this.slider, R.drawable.slider_indicator_circle);

        this.indicator.setPageCount(images.size());
        this.indicator.show();
    }

    private void clearFragments() {
        for (Fragment fragment:getChildFragmentManager().getFragments()) {
            getChildFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    private void openAssetActivity() {
        Intent i = new Intent(this.getActivity(), Asset.class);
        this.startActivity(i);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMainFragmentInteractionListener {
        // TODO: Update argument type and name
        void onMainFragmentInteraction(Object object);
        void afterMainViewInit();
    }
}
