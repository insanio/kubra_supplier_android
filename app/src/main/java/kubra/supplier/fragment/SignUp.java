package kubra.supplier.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import kubra.supplier.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignUp.OnSignUpFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignUp#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUp extends Fragment {

    private OnSignUpFragmentInteractionListener mListener;
    private View v;
    private Button buttonSignUp;
    private TextInputEditText textInputUsername, textInputPassword;

    public SignUp() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SignUp.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUp newInstance() {
        SignUp fragment = new SignUp();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v =  inflater.inflate(R.layout.fragment_sign_up, container, false);


        this.textInputUsername = this.v.findViewById(R.id.textinput_username);
        this.textInputPassword = this.v.findViewById(R.id.textinput_password);

        this.buttonSignUp = this.v.findViewById(R.id.button_signup);
        this.buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String username = textInputUsername.getText().toString();
                String password = textInputPassword.getText().toString();

                view.setEnabled(false);

                mListener.onSignUp(username, password, new AfterSignUpCallback() {
                    @Override
                    public void afterSignUp(Object success, Object failed) {
                        view.setEnabled(true);

                        textInputUsername.setText("");
                        textInputPassword.setText("");
                    }
                });
            }
        });

        this.mListener.afterSignUpFragmentInit();

        return this.v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignUpFragmentInteractionListener) {
            mListener = (OnSignUpFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSignUpFragmentInteractionListener {
        // TODO: Update argument type and name
        void afterSignUpFragmentInit();
        void onSignUp(String username, String password, AfterSignUpCallback callback);
    }

    public interface AfterSignUpCallback {
        void afterSignUp(Object success, Object failed);
    }
}
