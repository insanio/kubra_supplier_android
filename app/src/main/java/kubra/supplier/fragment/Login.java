package kubra.supplier.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import kubra.supplier.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnLoginFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Login#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Login extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    
    private OnLoginFragmentInteractionListener mListener;
    private View v;
    private Button buttonLogin, buttonRegister;
    private TextInputEditText textInputUsername, textInputPassword;

    public Login() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Login.
     */
    // TODO: Rename and change types and number of parameters
    public static Login newInstance() {
        Login fragment = new Login();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_login, container, false);

        this.textInputUsername = this.v.findViewById(R.id.textinput_username);
        this.textInputPassword = this.v.findViewById(R.id.textinput_password);

        this.buttonLogin = this.v.findViewById(R.id.button_login);
        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String username = textInputUsername.getText().toString();
                String password = textInputPassword.getText().toString();

                view.setEnabled(false);

                mListener.onLogin(username, password, new AfterLoginCallback() {
                    @Override
                    public void afterLogin(Object success, Object failed) {
                        view.setEnabled(true);

                        if (success != null) {
                            textInputUsername.setText("");
                            textInputPassword.setText("");
                        }
                    }
                });
            }
        });

        this.buttonRegister = this.v.findViewById(R.id.button_signup);
        this.buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onSignUp();
            }
        });

        this.mListener.afterLoginViewInit();

        return this.v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginFragmentInteractionListener {
        // TODO: Update argument type and name
        void afterLoginViewInit();
        void onLogin(String username, String password, AfterLoginCallback callback);
        void onRememberLogin(String username, String password);
        void onSignUp();
        void onExit();
    }

    public interface AfterLoginCallback {
        void afterLogin(Object success, Object failed);
    }
}
