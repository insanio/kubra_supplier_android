package kubra.supplier.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import kubra.supplier.R;
import kubra.supplier.utility.common.Screen;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile.OnProfileFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment {

    private OnProfileFragmentInteractionListener mListener;
    private View v;
    private LinearLayout containerAsset;
    private TextView textUsername, textName, textPhone, textMobile, textEmail, textAddress;
    private CircleImageView imageUser;
    private Button buttonLogout;

    private List<ProfileAssetViewHolder> holders;

    public Profile() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Profile.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile newInstance() {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_profile, container, false);

        this.containerAsset = this.v.findViewById(R.id.asset_container);

        this.buttonLogout = this.v.findViewById(R.id.button_logout);
        this.buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onLogout();
            }
        });

        this.imageUser = this.v.findViewById(R.id.image_user);
        this.imageUser.getLayoutParams().height = Screen.getScreenWidthPx(this.getContext());

        this.textUsername = this.v.findViewById(R.id.textview_username);
        this.textName = this.v.findViewById(R.id.textview_name);
        this.textPhone = this.v.findViewById(R.id.textview_phone);
        this.textMobile = this.v.findViewById(R.id.textview_mobile);
        this.textEmail = this.v.findViewById(R.id.textview_email);
        this.textAddress = this.v.findViewById(R.id.textview_address);

        this.holders = new ArrayList<>();

        this.mListener.afterProfileViewInit();

        return this.v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setProfile(Map<String, Object> profile) {
        this.textUsername.setText(profile.containsKey("username") ? profile.get("username").toString() : "");
        this.textName.setText(profile.containsKey("name") ? profile.get("name").toString() : "");
        this.textPhone.setText(profile.containsKey("phone") ? profile.get("phone").toString() : "");
        this.textMobile.setText(profile.containsKey("mobile") ? profile.get("mobile").toString() : "");
        this.textEmail.setText(profile.containsKey("email") ? profile.get("email").toString() : "");
        this.textAddress.setText(profile.containsKey("address") ? profile.get("address").toString() : "");
    }

    public void setAsset(List<Object> assets) {
        this.containerAsset.removeAllViews();

        for (Object asset:assets) {
            if (asset instanceof Map) {
//                Log.wtf("Profile Fragment", "Asset: " + asset);
                Map<String, Object> userAsset = (Map<String, Object>) asset;

                final ProfileAssetViewHolder view = new ProfileAssetViewHolder(LayoutInflater.from(this.getContext()).inflate(R.layout.fragment_profile_asset_list, null, false));
                view.setName(userAsset.get("name") == null ? "" : userAsset.get("name").toString());
                view.setDescription(userAsset.get("description") == null ? "" : userAsset.get("description").toString());
                view.setId(userAsset.get("server_id") == null ? "" : userAsset.get("server_id").toString());
                view.getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onProfileAssetClicked(view.getId());
                    }
                });

                this.holders.add(view);
            }
        }

        for (ProfileAssetViewHolder holder:this.holders) {
//            Log.wtf("Profile Fragment", "Holder: " + holder.getName());
            this.containerAsset.addView(holder.getView());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProfileFragmentInteractionListener {
        // TODO: Update argument type and name
        void afterProfileViewInit();
        void onProfileAssetClicked(String id);
        void onLogout();
    }
}

class ProfileAssetViewHolder {

    private View v;
    private TextView textName, textDescription;
    private String id;

    public ProfileAssetViewHolder(View v) {
        this.v = v;

        this.initView();
    }

    private void initView() {
        this.textName = this.v.findViewById(R.id.name);
        this.textDescription = this.v.findViewById(R.id.description);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    protected void setName(String name) {
        this.textName.setText(name);
    }

    protected String getName() {
        return this.textName.getText().toString();
    }

    protected void setDescription(String description) {
        this.textDescription.setText(description);
    }

    protected String getDescription() {
        return this.textDescription.getText().toString();
    }

    protected View getView() {
        return this.v;
    }
}