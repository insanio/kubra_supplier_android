package kubra.supplier.item;

public class Feed {

    private String id, title, feed, image, date, type_code, type_id, description;

    public Feed() {
        this.initFeed("", "", "", "", "", "", "", "");
    }

    public Feed(String id, String title, String feed, String image, String date, String type_id, String type_code, String description) {
        this.initFeed(id, title, feed, image, date, type_id, type_code, description);
    }

    private void initFeed(String id, String title, String feed, String image, String date, String type_id, String type_code, String description) {
        this.id = id;
        this.title = title;
        this.feed = feed;
        this.image = image;
        this.date = date;
        this.type_id = type_id;
        this.type_code = type_code;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType_code() {
        return type_code;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
