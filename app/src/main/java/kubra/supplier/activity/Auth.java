package kubra.supplier.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import kubra.supplier.R;
import kubra.supplier.fragment.Login;
import kubra.supplier.fragment.SignUp;
import kubra.supplier.service.AppSharedPreferences;
import kubra.supplier.service.Http;
import kubra.supplier.utility.common.AsyncTaskProcess;

public class Auth extends AppCompatActivity implements Login.OnLoginFragmentInteractionListener, SignUp.OnSignUpFragmentInteractionListener {

    @Override
    public void afterSignUpFragmentInit() {
    }

    @Override
    public void onSignUp(String username, String password, SignUp.AfterSignUpCallback callback) {
        this.threadRegisterProcess(username, password, callback);
    }

    @Override
    public void afterLoginViewInit() {
    }

    @Override
    public void onLogin(String username, String password, Login.AfterLoginCallback callback) {
        this.threadLoginProcess(username, password, callback);
    }

    @Override
    public void onRememberLogin(String username, String password) {
    }

    @Override
    public void onSignUp() {
        this.switchMode("signup");
    }

    @Override
    public void onExit() {
        this.finish();
    }

    private String mode;
    private List<AsyncTaskProcess> threads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        this.initView();
    }

    @Override
    protected void onDestroy() {
        this.stopAllThreads();
        this.setResult(RESULT_CANCELED);

        super.onDestroy();
    }

    @Override
    protected void onStop() {
        this.stopAllThreads();

        super.onStop();
    }

    @Override
    protected void onPause() {
        this.stopAllThreads();

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (this.getSupportFragmentManager().getBackStackEntryCount() == 1) {
            this.stopAllThreads();
            finish();
        }

        super.onBackPressed();
    }

    private void initView() {
        String mode = this.getIntent().getStringExtra("mode");
        this.threads = new ArrayList<>();

        this.switchMode(mode);
    }

    private void switchMode(String mode) {
        if (mode.equals("login")) {
            this.getSupportFragmentManager().beginTransaction().add(R.id.auth_container, Login.newInstance()).addToBackStack(null).commit();
        } else if (mode.equals("signup")) {
            this.getSupportFragmentManager().beginTransaction().replace(R.id.auth_container, SignUp.newInstance()).addToBackStack(null).commit();
        }

        this.mode = mode;
    }

    private void threadLoginProcess(final String username, final String password, final Login.AfterLoginCallback callback) {
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loginProcess(username, password, callback);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loginProcess(String username, String password, final Login.AfterLoginCallback callback) {
//        Log.wtf("Auth Activity", "Login process: " + username + ", " + password);

        JSONObject request = new JSONObject();
        try {
            request.put("username", username);
            request.put("password", password);

            Http.appPostJsonResponseJson(getApplicationContext(), "auth", new StringEntity(request.toString()), "application/json", new Http.AppHttpResponse() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, Object success) {
//                    Log.wtf("Auth Activity", "Login success!\n" + success);

                    try {
                        JSONObject response;
                        if (success instanceof JSONObject)
                            response = (JSONObject) success;
                        else
                            response = new JSONObject(success.toString());

                        if (response.getString("status").equals("success")) {
                            callback.afterLogin(success, null);

                            JSONObject data = response.getJSONObject("data");
                            String username = data.getJSONObject("login").getString("username");

                            AppSharedPreferences.getInstance().clearAllData(true);
                            AppSharedPreferences.getInstance().save(AppSharedPreferences.currentUsernameFieldName, username);
                            AppSharedPreferences.getInstance().save(AppSharedPreferences.tokenFieldName, data.getString("token"));
                            AppSharedPreferences.getInstance().save(AppSharedPreferences.aliasFieldName, data.getString("alias"));

                            Intent i = new Intent();
                            i.putExtra("username", username);
                            setResult(Activity.RESULT_OK, i);

                            finish();
                        } else {
                            Http.failedJsonResponseAction(getApplicationContext(), null, "Login gagal! Mohon periksa kembali Username / Password anda!", success);

                            callback.afterLogin(null, success);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                    Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to be having a problem processing this request...", failure);

                    callback.afterLogin(null, failure);
                }
            });
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void threadRegisterProcess(final String username, final String password, final SignUp.AfterSignUpCallback callback) {
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        registerProcess(username, password, callback);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void registerProcess(String username, String password, final SignUp.AfterSignUpCallback callback) {
        JSONObject request = new JSONObject();
        try {
            request.put("username", username);
            request.put("password", password);

            Http.appPostJsonResponseJson(getApplicationContext(), "auth/register", new StringEntity(request.toString()), "application/json", new Http.AppHttpResponse() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, Object success) {
//                    Log.wtf("Auth Activity", "Register success!\n" + success);

                    callback.afterSignUp(success, null);

                    onBackPressed();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                    Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to be having a problem processing this request...", failure);

                    callback.afterSignUp(null, failure);
                }
            });
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void stopAllThreads() {
        for (AsyncTaskProcess thread : this.threads) {
            if (!thread.isCancelled())
                thread.cancel(true);
        }

        this.threads.clear();
    }
}
