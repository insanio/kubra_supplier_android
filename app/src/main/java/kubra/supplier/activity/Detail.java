package kubra.supplier.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import kubra.supplier.R;
import kubra.supplier.fragment.Product;
import kubra.supplier.fragment.Profile;
import kubra.supplier.fragment.Store;
import kubra.supplier.service.AppSharedPreferences;
import kubra.supplier.service.Http;
import kubra.supplier.service.LocalDatabase;
import kubra.supplier.utility.common.AsyncTaskProcess;
import kubra.supplier.utility.common.Json;

public class Detail extends AppCompatActivity implements Profile.OnProfileFragmentInteractionListener, Store.OnStoreFragmentInteractionListener, Product.OnProductFragmentInteractionListener {

    @Override
    public void afterProductViewInit() {
        this.threadGetAssetData(this.tempId, "product");
    }

    @Override
    public void afterStoreViewInit() {
        this.threadGetAssetData(this.tempId, "store");
        this.threadGetAssetData(this.tempId, "product");
    }

    @Override
    public void onStoreAssetClicked(String id) {
        this.tempId = id;
        this.switchMode("product");
    }

    @Override
    public void afterProfileViewInit() {
        this.threadGetProfileData();
        this.threadGetAssetData(null, "store");
    }

    @Override
    public void onProfileAssetClicked(String id) {
        this.tempId = id;
        this.switchMode("store");
    }

    @Override
    public void onLogout() {
        this.logoutProcess();
    }

    private String mode, tempId;
    private List<AsyncTaskProcess> threads;
    private Profile profileFragment;
    private Store storeFragment;
    private Product productFragment;

    public Detail() {
        this.threads = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        this.initView();
    }

    @Override
    protected void onDestroy() {
        this.stopAllThreads();
        this.setResult(RESULT_CANCELED);

        super.onDestroy();
    }

    @Override
    protected void onStop() {
        this.stopAllThreads();

        super.onStop();
    }

    @Override
    protected void onPause() {
        this.stopAllThreads();

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (this.getSupportFragmentManager().getBackStackEntryCount() == 1) {
            this.stopAllThreads();
            finish();
        }

        super.onBackPressed();
    }

    private void initView() {
        String mode = this.getIntent().getStringExtra("mode");
        this.tempId = this.getIntent().getStringExtra("asset_id");
        this.threads = new ArrayList<>();

        this.profileFragment = Profile.newInstance();
        this.storeFragment = Store.newInstance();
        this.productFragment = Product.newInstance();

        this.switchMode(mode);
    }

    private void switchMode(String mode) {
        Log.wtf("Detail Activity", "Mode: " + mode + ", ID: " + this.tempId);
        if (mode.equals("user")) {
            this.getSupportFragmentManager().beginTransaction().add(R.id.detail_container, this.profileFragment).addToBackStack(null).commit();
        } else if (mode.equals("store")) {
            this.getSupportFragmentManager().beginTransaction().add(R.id.detail_container, this.storeFragment).addToBackStack(null).commit();
        } else if (mode.equals("product")) {
            this.getSupportFragmentManager().beginTransaction().add(R.id.detail_container, this.productFragment).addToBackStack(null).commit();
        }

        this.mode = mode;
    }

    private void threadGetProfileData() {
        final String id = AppSharedPreferences.getInstance().get(AppSharedPreferences.currentUsernameFieldName, "");

        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getProfileData(id);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getProfileData(final String id) {
//        ArrayList<ContentValues> profileData = LocalDatabase.getInstance(this.getApplicationContext()).loadAllData("user", new String[]{"name", "phone", "mobile", "email", "address"}, "username = ?", new String[]{id}, null, null, "name asc", "1");
//        if (profileData.size() == 0) {
        if (true) {
            JSONObject request = new JSONObject();
            try {
                request.put("username", id);

                StringEntity entity = new StringEntity(request.toString());

                Http.appPostJsonResponseJson(this.getApplicationContext(), "android/getuserdata", entity, "application/json", new Http.AppHttpResponse() {
                    public void onSuccess(int statusCode, Header[] headers, Object success) {
//                    Log.wtf("Main Activity", "Get user data success!\n" + success);

                        try {
                            JSONObject response;
                            if (success instanceof JSONObject)
                                response = (JSONObject) success;
                            else
                                response = new JSONObject(success.toString());

                            JSONObject user = response.getJSONObject("data").getJSONObject("user");
                            if (user.has("id")) {
                                java.util.Map<String, Object> userData = new HashMap<>();
                                userData.put("server_id", user.has("id") && user.get("id") != JSONObject.NULL ? user.get("id") : "");
                                userData.put("name", user.has("name") && user.get("name") != JSONObject.NULL ? user.get("name") : "");
                                userData.put("phone", user.has("phone") && user.get("phone") != JSONObject.NULL ? user.get("phone") : "");
                                userData.put("mobile", user.has("mobile") && user.get("mobile") != JSONObject.NULL ? user.get("mobile") : "");
                                userData.put("email", user.has("email") && user.get("email") != JSONObject.NULL ? user.get("email") : "");
                                userData.put("address", user.has("address") && user.get("address") != JSONObject.NULL ? user.get("address") : "");
                                userData.put("username", id);

                                profileFragment.setProfile(userData);

                                saveUserDataToLocalDatabase(id, userData);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                        Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to encountered a problem during request...", failure);

                        ArrayList<ContentValues> profileData = LocalDatabase.getInstance(getApplicationContext()).loadAllData("user", new String[]{"name", "phone", "mobile", "email", "address"}, "username = ?", new String[]{id}, null, null, "name asc", "1");
                        if (profileData.size() > 0) {
                            setProfileDataFromLocalDatabase(id, profileData.get(0));
                        }
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
//            this.setProfileDataFromLocalDatabase(id, profileData.get(0));
        }
    }

    private void setProfileDataFromLocalDatabase(String id, ContentValues profile) {
        java.util.Map<String, Object> userData = new HashMap<>();
        userData.put("server_id", profile.getAsString("server_id"));
        userData.put("name", profile.getAsString("name"));
        userData.put("phone", profile.getAsString("phone"));
        userData.put("mobile", profile.getAsString("mobile"));
        userData.put("email", profile.getAsString("email"));
        userData.put("address", profile.getAsString("address"));
        userData.put("username", id);

        profileFragment.setProfile(userData);
    }

    private void saveUserDataToLocalDatabase(String id, java.util.Map<String, Object> userData) {
        ContentValues cv = new ContentValues();
        for (java.util.Map.Entry element:userData.entrySet()) {
            Object value = element.getValue();
            value = value == JSONObject.NULL ? "" : value.toString();

            cv.put(element.getKey().toString(), value.toString());
        }
        cv.put("username", id);

        LocalDatabase.getInstance(this.getApplicationContext()).deleteData("user", "username = ?", new String[]{id}, true);
        LocalDatabase.getInstance(this.getApplicationContext()).insertData("user", cv, true);
    }

    private void threadGetAssetData(final String assetId, final String type) {
        final String id = AppSharedPreferences.getInstance().get(AppSharedPreferences.currentUsernameFieldName, "");

        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getAssetData(id, assetId, type);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getAssetData(final String id, final String assetId, final String type) {
//        ArrayList<ContentValues> assetData = LocalDatabase.getInstance(this.getApplicationContext()).loadAllData("asset", new String[]{"server_user_id", "name", "phone", "mobile", "email", "address", "latitude", "longitude", "description"}, "server_user_username = ?", new String[]{id}, null, null, "name asc", "1");
//        if (assetData.size() == 0) {
        if (true) {
            JSONObject request = new JSONObject();
            try {
                if (id != null && !id.equals(""))
                    request.put("username", id);
                request.put("asset_type", type);
                if (assetId != null && type.equals(this.mode))
                    request.put("id", assetId);
                else if (assetId != null && type.equals("product"))
                    request.put("parent_id", assetId);

                Log.wtf("Detail activity", "Get Asset: " + request);

                StringEntity entity = new StringEntity(request.toString());

                Http.appPostJsonResponseJson(this.getApplicationContext(), "android/getuserassetdata", entity, "application/json", new Http.AppHttpResponse() {
                    public void onSuccess(int statusCode, Header[] headers, Object success) {
//                        Log.wtf("Main Activity", "Get user data success!\n" + success);

                        try {
                            JSONObject response;
                            if (success instanceof JSONObject)
                                response = (JSONObject) success;
                            else
                                response = new JSONObject(success.toString());

                            JSONArray assets = response.getJSONObject("data").getJSONArray("data");
                            if (assets.length() > 0) {
                                List<Object> userAssetData = new ArrayList<>();

                                for (int i=0;i<assets.length();i++) {
                                    JSONObject asset = assets.getJSONObject(i);
                                    java.util.Map<String, Object> userData = new HashMap<>();
                                    userData.put("server_id", Json.validateJsonObject(asset, "id"));
                                    userData.put("server_user_id", Json.validateJsonObject(asset, "user_id"));
                                    userData.put("name", Json.validateJsonObject(asset, "name"));
                                    userData.put("phone", Json.validateJsonObject(asset, "phone"));
                                    userData.put("mobile", Json.validateJsonObject(asset, "mobile"));
                                    userData.put("email", Json.validateJsonObject(asset, "email"));
                                    userData.put("address", Json.validateJsonObject(asset, "address"));
                                    userData.put("latitude", Json.validateJsonObject(asset, "map_latitude"));
                                    userData.put("longitude", Json.validateJsonObject(asset, "map_longitude"));
                                    userData.put("server_user_username", Json.validateJsonObject(asset, "user_username"));
                                    userData.put("asset_type", Json.validateJsonObject(asset, "asset_type"));
                                    userData.put("asset_type_name", Json.validateJsonObject(asset, "asset_type_name"));
                                    userData.put("asset_type_code", Json.validateJsonObject(asset, "asset_type_code"));

                                    userAssetData.add(userData);
                                }

                                if (mode.equals("user"))
                                    profileFragment.setAsset(userAssetData);
                                else if (mode.equals("store")) {
                                    if (type.equals("store"))
                                        storeFragment.setStore((Map<String, Object>) userAssetData.get(0));
                                    else if (type.equals("product"))
                                        storeFragment.setAsset(userAssetData);
                                } else if (mode.equals("product")) {
                                    productFragment.setProduct((Map<String, Object>) userAssetData.get(0));
                                }

                                saveUserAssetDataToLocalDatabase(userAssetData);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                        Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to encountered a problem during request...", failure);

                        ArrayList<ContentValues> assetData = LocalDatabase.getInstance(getApplicationContext()).loadAllData("asset", new String[]{"server_user_id", "name", "phone", "mobile", "email", "address", "latitude", "longitude", "description"}, "server_user_username = ? and asset_type = ?", new String[]{id, type}, null, null, "name asc", "");
                        if (assetData.size() > 0)
                            setUserAssetDataFromLocalDatabase(type, assetData);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
//            this.setUserAssetDataFromLocalDatabase(id, assetData);
        }
    }

    private void setUserAssetDataFromLocalDatabase(String type, List<ContentValues> assetData) {
        List<Object> userAssetData = new ArrayList<>();
        for (ContentValues asset:assetData) {
            java.util.Map<String, Object> userData = new HashMap<>();
            userData.put("server_id", asset.get("id"));
            userData.put("server_user_id", asset.get("user_id"));
            userData.put("name", asset.get("name"));
            userData.put("phone", asset.get("phone"));
            userData.put("mobile", asset.get("mobile"));
            userData.put("email", asset.get("email"));
            userData.put("address", asset.get("address"));
            userData.put("latitude", asset.get("latitude"));
            userData.put("longitude", asset.get("longitude"));
            userData.put("server_user_username", asset.get("server_user_username"));
            userData.put("asset_type", asset.get("asset_type"));
            userData.put("asset_type_name", asset.get("asset_type_name"));
            userData.put("asset_type_code", asset.get("asset_type_code"));

            userAssetData.add(userData);
        }

        if (mode.equals("user"))
            profileFragment.setAsset(userAssetData);
        else if (mode.equals("store")) {
            if (type.equals("store"))
                storeFragment.setStore((Map<String, Object>) userAssetData.get(0));
            else if (type.equals("product"))
                storeFragment.setAsset(userAssetData);
        } else if (mode.equals("product")) {
            productFragment.setProduct((Map<String, Object>) userAssetData.get(0));
        }
    }

    private void saveUserAssetDataToLocalDatabase(List<Object> userAssetData) {
        ArrayList<ContentValues> contentValues = new ArrayList<>();
        List<String> condVals = new ArrayList<>();
        StringBuilder cond = new StringBuilder();
        for (Object userAsset:userAssetData) {
            ContentValues cv = new ContentValues();
            java.util.Map<String, Object> asset = (java.util.Map<String, Object>) userAsset;
            for (java.util.Map.Entry element : asset.entrySet()) {
                Object value = element.getValue();
                value = value == JSONObject.NULL ? "" : value.toString();

                cv.put(element.getKey().toString(), value.toString());

                if (!cond.toString().equals(""))
                    cond.append(", ");
                cond.append("?");
                condVals.add(value.toString());
            }

            contentValues.add(cv);
        }

        LocalDatabase db = LocalDatabase.getInstance(this.getApplicationContext());
        try {
            db.getWritableDatabase().beginTransaction();

            db.deleteData("asset", "server_id in (" + cond.toString() + ")", condVals.toArray(new String[]{}), false);
            db.insertAllData("asset", contentValues, false, false);

            db.getWritableDatabase().setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.getWritableDatabase().endTransaction();
        }
    }

    private void logoutProcess() {
        final String id = AppSharedPreferences.getInstance().get(AppSharedPreferences.currentUsernameFieldName, "");
        LocalDatabase.getInstance(this.getApplicationContext()).deleteData("user", "username = ?", new String[]{id}, true);

        AppSharedPreferences.getInstance().clearAllData(true);

        Intent intent = new Intent();
        intent.putExtra("process", "logout");

        this.setResult(RESULT_OK, intent);

        this.stopAllThreads();
        this.finish();
    }

    private void stopAllThreads() {
        for (AsyncTaskProcess thread : this.threads) {
            if (!thread.isCancelled())
                thread.cancel(true);
        }

        this.threads.clear();
    }
}
