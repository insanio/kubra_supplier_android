package kubra.supplier.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import kubra.supplier.App;
import kubra.supplier.R;
import kubra.supplier.fragment.Feed;
import kubra.supplier.fragment.Map;
import kubra.supplier.service.AppSharedPreferences;
import kubra.supplier.service.Http;
import kubra.supplier.service.LocalDatabase;
import kubra.supplier.utility.common.AsyncTaskProcess;
import kubra.supplier.utility.common.Json;

public class Asset extends AppCompatActivity implements Feed.OnFeedFragmentAction, Map.OnMapFragmentAction {

    @Override
    public void onFeedListSelected(kubra.supplier.item.Feed item) {
        callDetail(item);
    }

    @Override
    public void afterFeedViewInit() {
        this.getFeed();
    }

    @Override
    public void onMapAction(String uri) {
    }

    private ViewPager mainPager;
    private TabLayout mainTab;
    private MainPagerAdapter mainPagerAdapter;

    public boolean mapAccessFineLocation;
    private List<Fragment> fragments;
    private List<AsyncTaskProcess> threads;

    public Asset() {
        this.threads = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset);

        this.initView();
    }

    @Override
    protected void onPause() {
        this.stopAllThreads();

        super.onPause();
    }

    @Override
    protected void onStop() {
        this.stopAllThreads();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        this.stopAllThreads();

        super.onDestroy();
    }

    private void stopAllThreads() {
        for (AsyncTaskProcess thread : this.threads) {
            if (!thread.isCancelled())
                thread.cancel(true);
        }

        this.threads.clear();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        this.mapAccessFineLocation = false;
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.mapAccessFineLocation = true;

                    ((Map) this.fragments.get(1)).updateMap();
                }
            }
        }
    }

    public void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            this.mapAccessFineLocation = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }

        ((Map) this.fragments.get(1)).updateMap();
    }

    private void initView() {
        this.mainPager = this.findViewById(R.id.main_pager);
        this.mainTab = this.findViewById(R.id.main_tab);

        this.fragments = new ArrayList<>();
//        this.fragments.add(kubra.supplier.fragment.Main.newInstance());
        this.fragments.add(Feed.newInstance());
//        this.fragments.add(Feed.newInstance());
        this.fragments.add(Map.newInstance());

        this.mainPagerAdapter = new MainPagerAdapter(this.getApplicationContext(), this.getSupportFragmentManager(), fragments);

        this.mainPager.setAdapter(this.mainPagerAdapter);
        this.mainTab.setupWithViewPager(this.mainPager, true);

        this.mapAccessFineLocation = false;
    }

    private void getFeed() {
        this.threadGetAssetData(null);
    }

    private void threadGetAssetData(final String type) {
//        final String id = AppSharedPreferences.getInstance().get(AppSharedPreferences.currentUsernameFieldName, "");
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getAssetData(type);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getAssetData(final String type) {
//        ArrayList<ContentValues> assetData = LocalDatabase.getInstance(this.getApplicationContext()).loadAllData("asset", new String[]{"server_user_id", "name", "phone", "mobile", "email", "address", "latitude", "longitude", "description"}, "server_user_username = ?", new String[]{id}, null, null, "name asc", "1");
//        if (assetData.size() == 0) {
        if (true) {
            JSONObject request = new JSONObject();
            try {
                if (type != null)
                    request.put("type", type);

//                Log.wtf("Detail activity", "Get Asset: " + request);

                StringEntity entity = new StringEntity(request.toString());

                Http.appPostJsonResponseJson(this.getApplicationContext(), "android/getuserassetdata", entity, "application/json", new Http.AppHttpResponse() {
                    public void onSuccess(int statusCode, Header[] headers, Object success) {
//                        Log.wtf("Main Activity", "Get user data success!\n" + success);

                        try {
                            JSONObject response;
                            if (success instanceof JSONObject)
                                response = (JSONObject) success;
                            else
                                response = new JSONObject(success.toString());

                            JSONArray assets = response.getJSONObject("data").getJSONArray("data");
                            if (assets.length() > 0) {
                                List<Object> userAssetData = new ArrayList<>();

                                for (int i=0;i<assets.length();i++) {
                                    JSONObject asset = assets.getJSONObject(i);
                                    java.util.Map<String, Object> userData = new HashMap<>();
                                    userData.put("server_id", Json.validateJsonObject(asset, "id"));
                                    userData.put("server_user_id", Json.validateJsonObject(asset, "user_id"));
                                    userData.put("name", Json.validateJsonObject(asset, "name"));
                                    userData.put("phone", Json.validateJsonObject(asset, "phone"));
                                    userData.put("mobile", Json.validateJsonObject(asset, "mobile"));
                                    userData.put("email", Json.validateJsonObject(asset, "email"));
                                    userData.put("address", Json.validateJsonObject(asset, "address"));
                                    userData.put("latitude", Json.validateJsonObject(asset, "map_latitude"));
                                    userData.put("longitude", Json.validateJsonObject(asset, "map_longitude"));
                                    userData.put("server_user_username", Json.validateJsonObject(asset, "user_username"));
                                    userData.put("asset_type", Json.validateJsonObject(asset, "asset_type"));
                                    userData.put("asset_type_name", Json.validateJsonObject(asset, "asset_type_name"));
                                    userData.put("asset_type_code", Json.validateJsonObject(asset, "asset_type_code"));

                                    userAssetData.add(userData);
                                }

                                ((Feed) fragments.get(0)).setItem(userAssetData);

                                saveUserAssetDataToLocalDatabase(userAssetData);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                        Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to encountered a problem during request...", failure);

                        String cond = type == null ? "" : "asset_type = ?";
                        String condVals[] = type == null ? null : new String[]{type};
                        ArrayList<ContentValues> assetData = LocalDatabase.getInstance(getApplicationContext()).loadAllData("asset", new String[]{"server_user_id", "name", "phone", "mobile", "email", "address", "latitude", "longitude", "description"}, cond, condVals, null, null, "name asc", "");
                        if (assetData.size() > 0)
                            setUserAssetDataFromLocalDatabase(assetData);
                    }
                });
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
//            this.setUserAssetDataFromLocalDatabase(id, assetData);
        }
    }

    private void setUserAssetDataFromLocalDatabase(List<ContentValues> assetData) {
        List<Object> userAssetData = new ArrayList<>();
        for (ContentValues asset:assetData) {
            java.util.Map<String, Object> userData = new HashMap<>();
            userData.put("server_id", asset.get("id"));
            userData.put("server_user_id", asset.get("user_id"));
            userData.put("name", asset.get("name"));
            userData.put("phone", asset.get("phone"));
            userData.put("mobile", asset.get("mobile"));
            userData.put("email", asset.get("email"));
            userData.put("address", asset.get("address"));
            userData.put("latitude", asset.get("latitude"));
            userData.put("longitude", asset.get("longitude"));
            userData.put("server_user_username", asset.get("server_user_username"));
            userData.put("asset_type", asset.get("asset_type"));
            userData.put("asset_type_name", asset.get("asset_type_name"));
            userData.put("asset_type_code", asset.get("asset_type_code"));

            userAssetData.add(userData);
        }

        ((Feed) fragments.get(0)).setItem(userAssetData);
    }

    private void saveUserAssetDataToLocalDatabase(List<Object> userAssetData) {
        ArrayList<ContentValues> contentValues = new ArrayList<>();
        List<String> condVals = new ArrayList<>();
        StringBuilder cond = new StringBuilder();
        for (Object userAsset:userAssetData) {
            ContentValues cv = new ContentValues();
            java.util.Map<String, Object> asset = (java.util.Map<String, Object>) userAsset;
            for (java.util.Map.Entry element : asset.entrySet()) {
                Object value = element.getValue();
                value = value == JSONObject.NULL || value == null ? "" : value.toString();

                cv.put(element.getKey().toString(), value.toString());

                if (!cond.toString().equals(""))
                    cond.append(", ");
                cond.append("?");
                condVals.add(value.toString());
            }

            contentValues.add(cv);
        }

        LocalDatabase db = LocalDatabase.getInstance(this.getApplicationContext());
        try {
            db.getWritableDatabase().beginTransaction();

            db.deleteData("asset", "server_id in (" + cond.toString() + ")", condVals.toArray(new String[]{}), false);
            db.insertAllData("asset", contentValues, false, false);

            db.getWritableDatabase().setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.getWritableDatabase().endTransaction();
        }
    }

//    private void initMainFragment() {
//        ((kubra.supplier.fragment.Main) this.fragments.get(0)).setSliderImage(Arrays.asList("https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/2010-brown-bear.jpg/200px-2010-brown-bear.jpg", "https://defenders.org/sites/default/files/styles/homepage-feature-2015/public/grizzley-bear_jim-chagares.png?itok=WVinRHTJ", "https://cdn.images.express.co.uk/img/dynamic/78/590x/1006307_1.jpg?r=1534855004478"));
//    }

    private void callDetail(kubra.supplier.item.Feed feed) {
        this.stopAllThreads();

        Intent intent = new Intent(this, Detail.class);
        intent.putExtra("mode", feed.getType_code());
        intent.putExtra("asset_id", feed.getId());
        this.startActivityForResult(intent, App.detailRequest);
    }

    public class MainPagerAdapter extends FragmentPagerAdapter {

        private Context mContext;
        private List<Fragment> fragments;

        public MainPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.mContext = context;
            this.fragments = fragments;
        }

        // This determines the fragment for each tab
        @Override
        public Fragment getItem(int position) {
//            if (position == 0) {
//                return Feed.newInstance(1);
//            } else if (position == 1){
//                return Map.newInstance();
//            }

            if (position < this.fragments.size()) {
                return this.fragments.get(position);
            }

            return null;
        }

        // This determines the number of tabs
        @Override
        public int getCount() {
            return 2;
        }

        // This determines the title for each tab
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            switch (position) {
//                case 0:
//                    return "Dashboard";
                case 0:
                    return "Feed";
                case 1:
                    return "Map";
                default:
                    return null;
            }
        }
    }
}
