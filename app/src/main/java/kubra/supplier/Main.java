package kubra.supplier;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import kubra.supplier.activity.Auth;
import kubra.supplier.activity.Detail;
import kubra.supplier.service.AppSharedPreferences;
import kubra.supplier.service.Http;
import kubra.supplier.service.LocalDatabase;
import kubra.supplier.utility.common.AsyncTaskProcess;

public class Main extends AppCompatActivity implements kubra.supplier.fragment.Main.OnMainFragmentInteractionListener {

    @Override
    public void onMainFragmentInteraction(Object object) {
    }

    @Override
    public void afterMainViewInit() {
        this.initMainFragment();
    }

    private kubra.supplier.fragment.Main mainFragment;
    private AppCompatImageButton buttonLogin;
    private TextView textUserName;
    private List<AsyncTaskProcess> threads;
    private String currentUsername;

    public Main() {
        this.threads = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initView();
    }

    @Override
    protected void onPause() {
        this.stopAllThreads();

        super.onPause();
    }

    @Override
    protected void onStop() {
        this.stopAllThreads();

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        this.stopAllThreads();

        super.onDestroy();
    }

    private void stopAllThreads() {
        for (AsyncTaskProcess thread : this.threads) {
            if (!thread.isCancelled())
                thread.cancel(true);
        }

        this.threads.clear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == App.authRequest && resultCode == RESULT_OK && data != null) {
            this.currentUsername = data.getStringExtra("username");

            this.threadGetUserData(this.currentUsername);
        } else if (requestCode == App.detailRequest && resultCode == RESULT_OK && data != null) {
            if (data.hasExtra("process") && data.getStringExtra("process").equals("logout")) {
                this.currentUsername = "";
                this.textUserName.setText("");
            }
        }

//        this.threadGetBannerData();
    }

    private void initView() {
        this.currentUsername = AppSharedPreferences.getInstance().get(AppSharedPreferences.currentUsernameFieldName, "");

        this.buttonLogin = this.findViewById(R.id.login_image);
        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUsername.equals(""))
                    callAuth();
                else
                    callProfile();
            }
        });

        this.textUserName = this.findViewById(R.id.user_name);

        if (!this.currentUsername.equals("")) {
            this.getUserDataFromLocalDatabase();
        }
    }

    private void initMainFragment() {
        this.mainFragment = (kubra.supplier.fragment.Main) this.getSupportFragmentManager().findFragmentById(R.id.fragment_main);

        this.threadGetBannerData();
    }

    private void callAuth() {
        this.stopAllThreads();

        Intent intent = new Intent(this, Auth.class);
        intent.putExtra("mode", "login");
        this.startActivityForResult(intent, App.authRequest);
    }

    private void callProfile() {
        this.stopAllThreads();

        Intent intent = new Intent(this, Detail.class);
        intent.putExtra("mode", "user");
        this.startActivityForResult(intent, App.detailRequest);
    }

    private void threadGetBannerData() {
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getBannerData();
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    
    private void getBannerData() {
        JSONObject request = new JSONObject();
        try {
            StringEntity entity = new StringEntity(request.toString());

            Http.appPostJsonResponseJson(this.getApplicationContext(), "android/getbanner", entity, "application/json", new Http.AppHttpResponse() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, Object success) {
                    try {
                        JSONObject response;
                        if (success instanceof JSONObject)
                            response = (JSONObject) success;
                        else
                            response = new JSONObject(success.toString());

                        setBannerData(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                    Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to encountered a problem during request...", failure);
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void setBannerData(JSONObject response) {
        try {
            List<String> banners = new ArrayList<>();
            if (response.getString("status").equals("success")) {
                JSONArray bannerJson = response.getJSONObject("data").getJSONArray("data");
                for (int i = 0; i < bannerJson.length(); i++) {
                    JSONObject banner = bannerJson.getJSONObject(i);
                    banners.add(banner.getString("banner"));
                }

                if (banners.size() > 0) {
                    this.mainFragment.setSliderImage(banners);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void threadGetUserData(final String username) {
        AsyncTaskProcess thread = new AsyncTaskProcess(new AsyncTaskProcess.TaskProcess() {
            @Override
            public void process() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getUserData(username);
                    }
                });
            }
        }, this.threads);

        this.threads.add(thread);
        thread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getUserData(String username) {
        JSONObject request = new JSONObject();
        try {
            request.put("username", username);

            StringEntity entity = new StringEntity(request.toString());

            Http.appPostJsonResponseJson(this.getApplicationContext(), "auth/getuserdata", entity, "application/json", new Http.AppHttpResponse() {
                public void onSuccess(int statusCode, Header[] headers, Object success) {
//                    Log.wtf("Main Activity", "Get user data success!\n" + success);

                    try {
                        JSONObject response;
                        if (success instanceof JSONObject)
                            response = (JSONObject) success;
                        else
                            response = new JSONObject(success.toString());

                        JSONObject user = response.getJSONObject("data").getJSONObject("user");
                        if (user.has("id")) {
                            java.util.Map<String, Object> userData = new HashMap<>();
                            userData.put("server_id", user.get("id"));
                            userData.put("name", user.get("name"));
                            userData.put("phone", user.get("phone"));
                            userData.put("mobile", user.get("mobile"));
                            userData.put("email", user.get("email"));
                            userData.put("address", user.get("address"));

                            setUserData(userData);

                            saveUserDataToLocalDatabase(userData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable) {
                    Http.failedJsonResponseAction(getApplicationContext(), throwable, "Ooops... Server seems to encountered a problem during request...", failure);
                }
            });
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void saveUserDataToLocalDatabase(java.util.Map<String, Object> userData) {
        ContentValues cv = new ContentValues();
        for (java.util.Map.Entry element:userData.entrySet()) {
            Object value = element.getValue();
            value = value == JSONObject.NULL ? "" : value.toString();

            cv.put(element.getKey().toString(), value.toString());
        }
        cv.put("username", this.currentUsername);

        LocalDatabase.getInstance(this.getApplicationContext()).deleteData("user", "username = ?", new String[]{this.currentUsername}, true);
        LocalDatabase.getInstance(this.getApplicationContext()).insertData("user", cv, true);
    }

    private void getUserDataFromLocalDatabase() {
        ArrayList<ContentValues> profileData = LocalDatabase.getInstance(this.getApplicationContext()).loadAllData("user", new String[]{"name", "phone", "mobile", "email", "address"}, "username = ?", new String[]{this.currentUsername}, null, null, "name asc", "1");
        if (profileData.size() > 0) {
            ContentValues profile = profileData.get(0);

            this.textUserName.setText(profile.getAsString("name"));
        }
    }

    private void setUserData(java.util.Map<String, Object> userData) {
        this.textUserName.setText(userData.get("name").toString());
    }
}
