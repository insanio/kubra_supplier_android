package kubra.supplier.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;

public class Http {
    public static final String BASE_URL = "http://192.168.1.10/supplier_backend/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Http.client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Http.client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity entity, String contentType, AsyncHttpResponseHandler responseHandler) {
        Http.client.post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
    }
    
    public static void appPostJsonResponseJson(final Context context, String url, HttpEntity entity, String contentType, final AppHttpResponse httpResponse) {
        Log.d("Http service", "appPostJsonResponseJson: " + url + ", " + entity.toString());

        Http.post(context, url, entity, contentType, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                checkResponse(response);

                httpResponse.onFailure(statusCode, headers, response, null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                checkResponse(errorResponse);

                httpResponse.onFailure(statusCode, headers, errorResponse, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                checkResponse(errorResponse);

                httpResponse.onFailure(statusCode, headers, errorResponse, throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                checkResponse(responseString);

                httpResponse.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                checkResponse(responseString);

                httpResponse.onFailure(statusCode, headers, responseString, null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (checkResponse(response))
                    httpResponse.onSuccess(statusCode, headers, response);
                else
                    httpResponse.onFailure(statusCode, headers, response, null);
            }
        });
    }

    private static boolean checkResponse(Object object) {
        Log.d("Http Service", "Check response: " + object);

        if (object != null) {
            try {
                JSONObject response;
                if (object instanceof JSONObject) {
                    response = (JSONObject) object;
                } else {
                    response = new JSONObject(object.toString());
                }

                if (response.has("status") && response.getString("status").equals("fail_token")) {
                    return false;
                }

                if (response.has("token") && response.get("token") != JSONObject.NULL) {
                    AppSharedPreferences.getInstance().save(AppSharedPreferences.tokenFieldName, response.getString("token"));
                }
                if (response.has("alias") && response.get("alias") != JSONObject.NULL) {
                    AppSharedPreferences.getInstance().save(AppSharedPreferences.aliasFieldName, response.getString("alias"));
                }

                return true;
            } catch (JSONException e) {
                e.printStackTrace();

                return false;
            }
        } else {
            return false;
        }
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        String token = AppSharedPreferences.getInstance().get(AppSharedPreferences.tokenFieldName, "");
        String alias = AppSharedPreferences.getInstance().get(AppSharedPreferences.aliasFieldName, "");

        return Http.BASE_URL + relativeUrl + "?token=" + token + "&alias=" + alias;
    }

    public static void failedJsonResponseAction(Context context, Throwable error, String err, Object errorObject) {
        Toast.makeText(context, err, Toast.LENGTH_LONG).show();

        Log.wtf("Http", "Failed JSON: " + (error != null ? error.getMessage() : "No error") + ", " + errorObject);
    }

    public static Bitmap streamBitmapSync(final String bitmapUrl) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newSingleThreadExecutor();

        Future futureThread = pool.submit(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                URL url ;
                try {
                    url = new URL(bitmapUrl);
                    return BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        return (Bitmap) futureThread.get();
    }

    public static class StreamBitmapAsync extends AsyncTask<Object, Void, Bitmap> {

        StreamBitmapAsyncResponse response;

        public StreamBitmapAsync(StreamBitmapAsyncResponse response) {
            this.response = response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            String bitmapUrl = objects.length == 1 ? objects[0] == null ? "" : objects[0].toString() : "";

            if (!bitmapUrl.equals("")) {
                URL url;
                try {
                    url = new URL(objects[0].toString());
                    return BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            this.response.setBitmap(bitmap);
        }
    }

    public interface StreamBitmapAsyncResponse {
        void setBitmap(Bitmap bitmap);
    }
    
    public interface AppHttpResponse {
        void onSuccess(int statusCode, Header[] headers, Object success);
        void onFailure(int statusCode, Header[] headers, Object failure, Throwable throwable);
    }
}