package kubra.supplier.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;
import java.util.ArrayList;

import kubra.supplier.utility.database.Property;

public class LocalDatabase extends SQLiteOpenHelper {

    private static final String databaseName = "kubra_supplier";
    private static final int databaseVersion = 1;
    private static LocalDatabase db;
    private Property dbProperty;

    public synchronized static LocalDatabase getInstance(Context context) {
        if (db == null) {
            db = new LocalDatabase(context, new Property());
        }

        return db;
    }

    public LocalDatabase(Context context, Property dbProperty) {
        super(context, databaseName, null, databaseVersion);

        this.dbProperty = dbProperty;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.dbProperty.createAllTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        this.dbProperty.dropAllTable(sqLiteDatabase);
        this.dbProperty.createAllTable(sqLiteDatabase);
    }

    /**
     * Tries to insert c session with description
     * @param cv
     * @throws SQLException on insert error
     */
    public void insertData(String tab, ContentValues cv, boolean autoTrans) {
        final SQLiteDatabase writableDatabase = getWritableDatabase();
        this.dbProperty.checkSQLTable(writableDatabase, tab);

        if (autoTrans) {
            writableDatabase.beginTransaction();

            try {
                writableDatabase.insertOrThrow(tab, null, cv);

                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        } else
            writableDatabase.insertOrThrow(tab, null, cv);
    }

    /**
     * Tries to insert c session with description
     * @param cv
     * @throws SQLException on insert error
     */
    public void insertAllData(String tab, ArrayList<ContentValues> cv, boolean autoTrans, boolean deleteAllData) throws SQLException {
        final SQLiteDatabase writableDatabase = getWritableDatabase();
        this.dbProperty.checkSQLTable(writableDatabase, tab);

        if (deleteAllData) {
            deleteData(tab, null, null, autoTrans);
        }

        if (autoTrans) {
            writableDatabase.beginTransaction();

            try {
                for (ContentValues c : cv)
                    writableDatabase.insertOrThrow(tab, null, c);

                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        } else {
            for (ContentValues c : cv)
                writableDatabase.insertOrThrow(tab, null, c);
        }
    }

    /**
     * Tries to insert c session with description
     * @param cond
     * @throws SQLException on insert error
     */
    public void deleteData(String tab, String cond, String condVals[], boolean autoTrans) {
        final SQLiteDatabase writableDatabase = getWritableDatabase();
        this.dbProperty.checkSQLTable(writableDatabase, tab);

        if (autoTrans) {
            writableDatabase.beginTransaction();

            try {
                writableDatabase.delete(tab, cond, condVals);

                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        } else
            writableDatabase.delete(tab, cond, condVals);
    }

    /**
     *
     * @return Count of session records or -1 on error
     */
    public int getCount(String tab, String cond, String[] values) {
        cond = cond == null ? "" : cond;
        int count = -1;
        SQLiteDatabase readableDatabase = getReadableDatabase();
        this.dbProperty.checkSQLTable(readableDatabase, tab);

        Cursor cursor = readableDatabase.rawQuery(" select count(*) from " + tab + " where 1=1 " + cond, values);
        if(cursor.moveToFirst())
            count = cursor.getInt(0);
        cursor.close();

        return count;
    }

    /**
     * Update record by ID with description
     * @param cond
     * @param condVals
     * @param tab
     * @param cv
     */
    public void updateData(String tab, String cond, String condVals[], ContentValues cv, boolean autoTrans) throws SQLException {
        final SQLiteDatabase writableDatabase = getWritableDatabase();
        this.dbProperty.checkSQLTable(writableDatabase, tab);

        if (autoTrans) {
            writableDatabase.beginTransaction();

            try {
                writableDatabase.update(tab, cv, cond, condVals);

                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        } else
            writableDatabase.update(tab, cv, cond, condVals);
    }

    /**
     *
     * @return All db records
     */
    public ArrayList<ContentValues> loadAllData(String tab, String cols[], String cond, String condVals[],
                                                String group, String having, String order, String limit) {
        final ArrayList<ContentValues> cv = new ArrayList<>();
        final SQLiteDatabase readableDatabase = getReadableDatabase();
        this.dbProperty.checkSQLTable(readableDatabase, tab);
        final Cursor cursor = readableDatabase.query(tab, cols,
                cond, condVals, group, having, order, limit);

        try {
            while (cursor.moveToNext()) {
                final ContentValues c = new ContentValues();

                for (int i=0;i<cursor.getColumnCount();i++) {
                    c.put(cols[i], cursor.getString(i));
                }

                cv.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return cv;
    }
}
