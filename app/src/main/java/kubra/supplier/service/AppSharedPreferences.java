package kubra.supplier.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;

import kubra.supplier.App;

public class AppSharedPreferences {
    private static final String TAG = "SharedPrefsHelper";
    public static final String tokenFieldName = "token", aliasFieldName = "alias", currentUsernameFieldName = "current_username";
    private static final String sharedPreferencesName = "rekomine";

    private static AppSharedPreferences instance;
    private SharedPreferences sharedPreferences;

    public static synchronized AppSharedPreferences getInstance() {
        if (instance == null) {
            instance = new AppSharedPreferences();
        }

        return instance;
    }

    private AppSharedPreferences() {
        instance = this;

        if(App.getInstance() == null){
            Log.e(TAG,"App instance is null");
        }else{
            Log.d(TAG,"App instance is not null");
        }

        sharedPreferences = App.getInstance().getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE);
    }

    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            getEditor().remove(key).commit();
        }
    }

    public void save(String key, Object value) {
        SharedPreferences.Editor editor = getEditor();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value instanceof ArrayList) {
            editor.putStringSet(key, new HashSet<>((ArrayList<String>) value));
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.commit();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) sharedPreferences.getAll().get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public boolean has(String key) {
        return sharedPreferences.contains(key);
    }

    public void clearAllData(boolean clearToken) {
        String tokenTemp = null;
        SharedPreferences.Editor editor = getEditor();

        if (!clearToken)
            tokenTemp = get(tokenFieldName, "");
        editor.clear().commit();

        if (tokenTemp != null && !tokenTemp.equals(""))
            save(tokenFieldName, tokenTemp);
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }
}
